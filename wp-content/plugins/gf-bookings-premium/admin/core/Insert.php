<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) { exit; } 



global $wpdb;
$gfb_email_configuration = $wpdb->prefix."gfb_email_configuration";
$gfb_createdby = get_current_user_id();
$gfb_ip = $_SERVER["SERVER_ADDR"];

$notifications = array(
	array(
		"label"   => esc_html__( "Email to customer about appointment confirmation", "gfb" ),
		"title"   => "gfb_user_appointment_confirmation",
		"subject" => esc_html__( "Notification About Appointment Confirmation ", "gfb" ),
		"message" => wpautop( esc_html__( "Dear {client_name}, \n\nThis is just an email to confirm your appointment with {staff_name}. For reference, here's the appointment information: \n\n Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name} \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to customer about visit confirmation", "gfb" ),
		"title"   => "gfb_user_visit_confirmation",
		"subject" => esc_html__( "Thank You For Visit", "gfb" ),
		"message" => wpautop( esc_html__( "Dear {client_name}, \n\n This is to thank you for your visit at our place. We hope that you were satisfied with our services. \n If any issue or query, you are free to call us. \ Thank You.  \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to customer about appointment cancellation", "gfb" ),
		"title"   => "gfb_user_appointment_cancellation",
		"subject" => esc_html__( "Notification About Appopintment Cancellation", "gfb" ),
		"message" => wpautop( esc_html__( "Dear {client_name}, \n\nThis is just an email to inform you that your appointment with {staff_name} has been cancelled. For reference, here's the appointment information: \n\n Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name}  \n Staff: {staff_name} . \n We regret about your inconvenience from our side. \n Thank You \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to admin about customer visit", "gfb" ),
		"title"   => "gfb_admin_customer_visit",
		"subject" => esc_html__( "Notification About Customer Visit", "gfb" ),
		"message" => wpautop( esc_html__( "Dear Administrator, \n\nThis is an email inform you about cancallation about the appointment with {staff_name}. For reference, here's the appointment information: \n\n Client Name: {client_name} Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name}. \n Thank You \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to admin about appointment request", "gfb" ),
		"title"   => "gfb_admin_appointment_request",
		"subject" => esc_html__( "Notification About Appointment Request", "gfb" ),
		"message" => wpautop( esc_html__( "Dear Administrator, \n\nThis is an email to inform you about a new appointment request. For reference, here's the appointment information: \n\n Client Name: {client_name} Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name} \n Thank You. \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to admin about appointment cancellation", "gfb" ),
		"title"   => "gfb_admin_appointment_cancellation",
		"subject" => esc_html__( "Notification About Appointment Cancellation", "gfb" ),
		"message" => wpautop( esc_html__( "Dear Administrator, \n\nThis is an email to inform you about an appointment has been cancelled. For reference, here's the appointment information: \n\n Client Name: {client_name} Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name} \n Thank You. \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to staff about appointment cancellation", "gfb" ),
		"title"   => "gfb_staff_appointment_cancelled",
		"subject" => esc_html__( "Notification About Appointment Cancellation", "gfb" ),
		"message" => wpautop( esc_html__( "Dear {staff_name}, \n\nThis is an email to inform you about an appointment has been cancelled. For reference, here's the appointment information: \n\n Client Name: {client_name} Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name} \n Thank You. \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
	array(
		"label"   => esc_html__( "Email to staff about appointment request", "gfb" ),
		"title"   => "gfb_staff_appointment_request",
		"subject" => esc_html__( "Notification About Appointment Request", "gfb" ),
		"message" => wpautop( esc_html__( "Dear {staff_name}, \n\nThis is an email to inform you about a new  appointment request for you. For reference, here's the appointment information: \n\n Client Name: {client_name} Date: {appointment_date} \n Time: {appointment_time} \n Service: {service_name} \n Staff: {staff_name} \n Thank You. \n\n Sincerely, \n{company_name} , {company_phone} \n {company_email} \n {company_website}", "gfb" ) ),
	),
);

foreach($notifications as $key => $notification) {
	
	$check_exists = $wpdb->get_results("SELECT email_config_id FROM ".$gfb_email_configuration." WHERE title='".$notification["title"]."' and is_deleted=0");
	
	if( empty($check_exists) ) {
		
		$insert_arg = array(
			'label'  		=> ''.wp_kses_post($notification["label"]).'',
			'title'  		=> ''.wp_kses_post($notification["title"]).'',
			'subject' 		=> ''.wp_kses_post($notification["subject"]).'',
			'message' 		=> ''.wp_kses_post($notification["message"]).'',
			'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
			'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
			'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
		);
		
		$insert_emails = $wpdb->insert($gfb_email_configuration, $insert_arg);
		
	}
}