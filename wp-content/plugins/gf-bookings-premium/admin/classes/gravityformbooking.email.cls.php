<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingEmail {
		
		function __construct() {
			add_action( "wp_ajax_gfb_add_mail", array(&$this, "gfbUpdateEmailNotifications") );	
		}
		
		/* default codes for email */
		function gfbCodes() {
			
			return apply_filters( "gfbCodes", array(
				"{appointment_date}"    => esc_html__( "date of appointment", "gfb" ),
				"{appointment_time}"    => esc_html__( "time of appointment", "gfb" ),
				"{booking_ref_number}"  => esc_html__( "booking reference number of appointment", "gfb" ),
				"{client_email}"        => esc_html__( "email of client", "gfb" ),
				"{client_name}"  		=> esc_html__( "name of client", "gfb" ),
				"{client_phone}"		=> esc_html__( "contact no of client", "gfb" ),
				"{company_address}" 	=> esc_html__( "address of company", "gfb" ),
				"{company_name}" 		=> esc_html__( "name of company", "gfb" ),
				"{company_email}" 		=> esc_html__( "company email", "gfb" ),
				"{company_phone}" 		=> esc_html__( "company phone", "gfb" ),
				"{company_website}" 	=> esc_html__( "company website", "gfb" ),
				"{service_name}" 		=> esc_html__( "name of service", "gfb" ),
				"{staff_name}" 			=> esc_html__( "name of staff", "gfb" ),
				"{total_price}" 		=> esc_html__( "total price of booking", "gfb" ),
			) );
		}
		
		/* fetch all notifications */
		function gfbGetNotifications() {
			global $wpdb;
			$gfb_email_configuration = $wpdb->prefix."gfb_email_configuration";
			
			$email_result = $wpdb->get_results("SELECT email_config_id, label, title, subject, message FROM ".$gfb_email_configuration." WHERE is_deleted=0", ARRAY_A);
			return $email_result;				
		}
		
		/* fetch notification from id */
		function gfbGetNotificationDetails( $client_notify_id = '', $staff_notify_id = '', $admin_notify_id = '' ) {
			global $wpdb;
			$gfb_email_configuration = $wpdb->prefix."gfb_email_configuration";
			
			
			$email_result = $wpdb->get_results("SELECT em1.title as userTitle, em1.subject as userSubject, em1.message as userMessage, em2.title as adminTitle, em2.subject as adminSubject, em2.message as adminMessage, em3.title as staffTitle, em3.subject as staffSubject, em3.message as staffMessage 
FROM ".$gfb_email_configuration." AS em1 
LEFT JOIN ".$gfb_email_configuration." AS em2 ON em2.title='".$admin_notify_id."'
LEFT JOIN ".$gfb_email_configuration." AS em3 ON em3.title='".$staff_notify_id."'
WHERE em1.title='".$client_notify_id."'", ARRAY_A);
			return $email_result;				
		}
		
		/* Add data in table */
		function gfbUpdateEmailNotifications(){
			
			parse_str($_POST["form_data"], $enotification);
			//print_r($enotification); exit;
			global $wpdb;
			$gfb_email_configuration = $wpdb->prefix."gfb_email_configuration";
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			$counter = 0;
			
			$countNotification = count($this->gfbGetNotifications());
			
			for( $i=1; $i<=$countNotification; $i++ ) {
				
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT title FROM ".$gfb_email_configuration." WHERE is_deleted=0 AND title='".$enotification['title_'.$i]."' AND email_config_id ='".base64_decode($enotification['email_config_id_'.$i])."'", ARRAY_A );
								
				if( !empty( $check_exists ) ) 
				{
					$email_args = array(
						'subject' 		=> ''.wp_kses_post(stripslashes($enotification['subject_'.$i])).'',
						'message' 		=> ''.wp_kses_post(stripslashes($enotification['message_'.$i])).'',
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
									
					
					$where_args = array(
						'title' 		  => ''.$enotification['title_'.$i].'',
						'email_config_id' => ''.base64_decode($enotification['email_config_id_'.$i]).'',
					);
					
					$update_email = $wpdb->update($gfb_email_configuration, $email_args, $where_args);					
					$counter++;
				}	
				
			}
			
			if( $countNotification == $counter )
			{		
				echo json_encode( array('msg' => 'true', 'text' => 'Notifications saved successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-email') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Notifications not saved successfully.') );
				die();
			}				
			
			die();	
		}
		
	}
	
	global $gfbEmail;
	$gfbEmail = new GravityFormBookingEmail;
