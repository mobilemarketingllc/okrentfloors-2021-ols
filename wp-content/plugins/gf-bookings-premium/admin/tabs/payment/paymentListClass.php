<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Payment_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'payment',     //singular name of the listed records
				'plural'    => 'payments',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		
		function get_columns()
		{
			$columnNames = array(
				//'payment_id' => 'payment_id',
				'customer_name' => 'Customer Name',
				'customer_email' => 'Customer Email',
				'booking_ref_no' => 'Booking Number',		
				'payment_date' => 'Payment Date',				
				'total_amt' => 'Total Amount',				
				'payment_status' => 'Payment Status',
				'payment_status_change' => 'Change Status',
			);
			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{

			// echo '<pre>$item$item';
			// print_r( $item );
			// echo '</pre>';
			switch( $column_name ) 
			{ 
				case 'customer_name':
				case 'customer_email':
				case 'booking_ref_no':
				case 'payment_date':
				case 'total_amt':
					return $item[ $column_name ];
				case 'payment_status':
					if( $item["payment_status"] == 1 ){
						return '<span class="gfb_badge badge-green">Completed</span>';
					}
					elseif( $item["payment_status"] == 2 ){
						return '<span class="gfb_badge badge-red">Cancelled</span>';
					} elseif ( $item["payment_status"] == 0 ) {
						return '<span class="gfb_badge badge-yellow">Pending</span>';
					}
				case 'payment_status_change':
					if ( $item["payment_status"] == 0 ) {
							$options = '							
								<option value="1">Completed</option>
								<option value="2">Cancelled</option>
							';
						} elseif ( $item["payment_status"] == 1 ) {
							$options = '
								<option value="0">Pending</option>							
								<option value="2">Cancelled</option>
							';
						} elseif ( $item["payment_status"] == 2 ) {
							$options = '
								<option value="0">Pending</option>
								<option value="1">Completed</option>							
							';
						}
						
						return '
						<input type="hidden" class="payment_id" name="payment_id" value="'. $item["payment_id"] .'"/>
						<select name="payment_status_change" class="payment_status_change">
							<option>--select status--</option>
							'. $options .'
						</select>';			
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function get_sortable_columns() {}
		
		
		function prepare_items() 
		{
			global $wpdb;
			$perPage = 10;
			$currentPage = $this->get_pagenum();
			
			$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
			$gfb_appointments_mst = $wpdb->prefix . "gfb_appointments_mst";
			$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
	
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			
			if( isset($_GET['paynm-filter']) && $_GET['paynm-filter'] <> '' ) 
			{				
				$filter = " AND cm.customer_id LIKE '".base64_decode($_GET['paynm-filter'])."%'";			
			}
			elseif( isset($_GET['paytrans-filter']) && $_GET['paytrans-filter'] <> '' ) 
			{
				$filter = " AND pm.transaction_id LIKE '".$_GET['paytrans-filter']."%'";			
			}else {				
				$filter = '';
			}
			
			$orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($sortable))) ? $_REQUEST['orderby'] : 'payment_id';
			
			$order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';
			
			$payment_results = $wpdb->get_results("SELECT am.booking_ref_no,pm.payment_id, pm.transaction_id, pm.appointment_id, DATE_FORMAT(pm.payment_date, '%d, %M %Y %H:%i') as payment_date , CONCAT(COALESCE(pm.currency_code,''),' ',pm.total_amt) as total_amt, cm.customer_name, cm.customer_email, pm.payment_type, pm.payment_status , cm.customer_id
			FROM ".$gfb_payments_mst." as pm
			INNER JOIN ".$gfb_appointments_mst." as am ON pm.appointment_id=am.appointment_id
			INNER JOIN ".$gfb_customer_mst." as cm ON am.customer_id=cm.customer_id
			WHERE pm.is_deleted=0 ".$filter." ORDER BY ".$orderby." ".$order."", ARRAY_A);
		
			$totalItems = count($payment_results);
			
			$payment_rows = array_slice($payment_results,(($currentPage-1)*$perPage),$perPage);
						
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage) // calculate pages count
			));
			
			$this->items = $payment_rows;
		}
	}

