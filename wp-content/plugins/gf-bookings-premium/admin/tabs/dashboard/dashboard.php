<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

	/* License Verification */
	

	global $gfbEmail;
	$notifyList = $gfbEmail->gfbGetNotifications();
	
	global $charts;
	
	/* Total Customers */
	$customers = $charts->gfbGetTotalCustomers();
	
	/* Total Services */
	$services = $charts->gfbGetTotalServices();
	
	/* Total Staff */
	$staff = $charts->gfbGetTotalStaff();
	
	/* Total Collection */
	$collections = $charts->gfbGetTodayTotalCollection(date("Y-m-d"));	
?>
<div>
	<div class="gfb-highpoints main_section">
		<div class="small-box bg-green">
			<div class="inner">
				<div class="icon">
					<i class="fc-white dashicons dashicons-groups"></i>
				</div> 
				<h1><?php _e("Customers", "gfb"); ?></h1>          	
				<p><?php 
					if($customers[0]["total_customer"] <> '') {
						echo $customers[0]["total_customer"];
					}
					else {
						echo '0';	
					}
					?></p> 
			</div>
		</div>
		
		<div class="small-box bg-blue">
			<div class="inner">
				<div class="icon">
					<i class="fc-white dashicons dashicons-admin-network"></i>
				</div> 
				<h1><?php _e("Services", "gfb"); ?></h1>          	
				<p><?php 
					if($services[0]["total_services"] <> '') {
						echo $services[0]["total_services"];
					}
					else {
						echo '0';	
					}
					?></p> 
			</div>
		</div>
		
		<div class="small-box bg-orange">
			<div class="inner">
				<div class="icon">
					<i class="fc-white dashicons dashicons-businessman"></i>
				</div>
				<h1><?php _e("Staff", "gfb"); ?></h1> 
				<p><?php 
					if($staff[0]["total_staff"] <> '') {
						echo $staff[0]["total_staff"];
					}
					else {
						echo '0';	
					}
					?></p>         	
			</div>
		</div>
		
		<div class="small-box bg-red">
			<div class="inner">
				<div class="icon">
					<i class="fc-white dashicons dashicons-money"></i>
				</div>

				<h1><?php _e("Today's Collection", "gfb"); ?></h1> 
				<p><?php 
					if($collections[0]["total_collection"] <> '') {
						echo get_option('gfb_currency_symbol').$collections[0]["total_collection"];
					}
					else {
						echo get_option('gfb_currency_symbol').'0';	
					}
					?></p> 
			</div>
		</div>
	</div>

	<div class="gfb-charts main_section">
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Payment Details Chart", "gfb"); ?></h3>
			<canvas  id="paymentChartDiv" class="gfbchartdiv"></canvas>
		</div>
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Appointment Status Chart", "gfb"); ?></h3>
			<canvas id="appointmentStatusChartDiv" class="gfbchartdiv"></canvas>
		</div>
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Staff Wise Earning", "gfb"); ?></h3>
			<canvas id="earningChartStaff" class="gfbchartdiv"></canvas>
		</div>
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Staff Wise Total no. of Appointments", "gfb"); ?></h3>
			<canvas id="bookingChartStaff" class="gfbchartdiv"></canvas>
		</div>
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Service Wise Total Earning", "gfb"); ?></h3>
			<canvas id="bookingChartService" class="gfbchartdiv"></canvas>
		</div>
		<div class="gfb-friendly-charts main_col">
			<h3><?php _e("Service Wise Total no of Appointments", "gfb"); ?></h3>
			<canvas id="earningChartService" class="gfbchartdiv"></canvas>
		</div>
	</div>
</div>