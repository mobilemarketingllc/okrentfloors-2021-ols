<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Services', 'gfb'); ?></h2>
            
			<div class="gfb_maintab-options">
				<a href="#serviceHtml" name="add_service" id="add_service" class="add-service-modal btn-add-element button button-secondary"><?php _e('+ New Service', 'gfb'); ?></a>
			</div>  
            
            <div id="serviceHtml" class="popup-modal mfp-hide white-popup">
            	<div class="gfbAjaxLoader" id="gfb_loader_img">
                    <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>
				<?php require_once( GFB_ADMIN_TAB . 'services/addService.php' );  ?>
            </div>
                
            <div class="popup-modal mfp-hide" id="editServiceHtml">
            
            	<div class="gfbAjaxLoader" id="gfb_loader_img">
                    <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>
            
                <div class="popup-block-main">
                    <div class="popup-block-main-title"><?php _e('Edit Service', 'gfb'); ?></div>
                </div>
                
                <div class="popup-block-main-body">
                    <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>                 

            </div>
		</div>
        
         <div class="gfb_maintab-content-search filter-box">
         
         	<?php
         	/* CATEGORY FILTER */
            global $gfbCategoryObj;
            $categoryname = $gfbCategoryObj->gfbListCategories();
            ?>
            <select name="category-filter" class="gfb-filter-category chosen-select">
                <option value=""><?php _e("Category Name", "gfb"); ?></option>
                <option value="*"><?php _e("All", "gfb"); ?></option>
                <?php
                foreach( $categoryname as $category ){
                    $selected = '';
                    if( base64_decode($_GET['category-filter']) == $category['category_id'] ){
                        $selected = ' selected = "selected"';   
                    }                    
                ?>
                <option value="<?php echo base64_encode($category['category_id']); ?>" <?php echo $selected; ?>><?php echo $category['category_name']; ?></option>
                <?php  
                }
                ?>
            </select>
            
            <!-- Service Filter -->
            <form name="gfb_service_name_filter" id="gfb_service_name_filter">
                <div class="search-box">
                    <input type="text" name="gfb_service_nm_filter" id="gfb_service_nm_filter" class="form-control" placeholder="Service Name" value="<?php if( isset($_GET['servicenm-filter']) ) { echo $_GET['servicenm-filter']; } ?>" />
                    
                    <button class="gfb_search" type="button" name="gfb_service_nm_submit" id="gfb_service_nm_submit"><span class="dashicons dashicons-search"></span></button>
            	</div>
            </form>
            
            <!-- Price Filter -->
           
            <form name="gfb_service_price_filter" id="gfb_service_price_filter">
                <div class="search-box">
                	<input type="text" name="gfb_price_filter" id="gfb_price_filter" class="form-control" placeholder="Price Value" value="<?php if( isset($_GET['price-filter']) ) { echo $_GET['price-filter']; } ?>" />                
                	<button class="gfb_search" type="button" name="gfb_service_price_submit" id="gfb_service_price_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
            
            <!-- Export to CSV -->	
            <form name="export_service" id="export_service" method="get"> 
                
                <button type="submit" name="gfb_service_export_to_csv" id="gfb_service_export_to_csv" class="gfb_export_csv" value="gfb_export_csv"><span class="dashicons dashicons-download"></span><?php _e('CSV', 'gfb'); ?></button>
                
                <button type="submit" name="gfb_service_export_to_pdf" id="gfb_service_export_to_pdf" class="gfb_export_pdf" value="gfb_export_pdf"><span class="dashicons dashicons-download"></span><?php _e('PDF', 'gfb'); ?></button>       	            
            </form>
        </div>		
    
		<div class="gfb_maintab-content">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
			
			<form method="post" name="service_list_form" id="service_list_form">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php 
					require_once( GFB_ADMIN_TAB . 'services/serviceListClass.php');
					$service_data = new Service_List_Tbl;
					$service_data->prepare_items();
					$service_data->display(); 
				?>
            </form>

		</div>

	</div>

</div><!-- /wrap -->