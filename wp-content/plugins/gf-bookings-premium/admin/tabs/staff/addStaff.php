<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?> 
<div class="popup-block-main">

    <div class="popup-block-main-title"><?php _e('Add Staff', 'gfb'); ?></div>
    
    <div class="popup-block-main-body">
    
        <form name="add_staff_form" class="staff-form" id="add_staff_form" method="post"> 
        
            <div class="form-section">
            
            	<div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="staff_name"><?php _e('Staff Full Name', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="staff_name" id="staff_name" class="input-main notallowspecial" maxlength="100" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="staff_username"><?php _e('Staff Username', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="staff_username" id="staff_username" class="input-main" maxlength="15" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="staff_email"><?php _e('Staff Email', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="email" name="staff_email" id="staff_email" class="input-main" maxlength="150" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="staff_password"><?php _e('Staff Password', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="password" name="staff_password" id="staff_password" class="input-main" maxlength="10" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                
                    <div class="form-element">
                        <?php wp_nonce_field('staff_nonce_field', 'staff_nonce', true, true); ?>
                        <?php submit_button('Save'); ?>
                    </div>
                
                </div>
                
            </div>
            
        </form>
    
    </div>

</div>   