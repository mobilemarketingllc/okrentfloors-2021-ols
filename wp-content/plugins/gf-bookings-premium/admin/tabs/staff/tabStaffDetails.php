<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	global $gfb_objCalendar;	
	$staff_id = base64_decode($_GET["state"]);	
	$auth_url = $gfb_objCalendar->executeEditStaff($staff_id);
	
?> 
<h4 class="gfb_section-title"><?php _e("Staff Member Details", "gfb"); ?></h4>

<form name="detail_staff_form" class="staff-form" id="detail_staff_form" method="post"> 

    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Full Name", "gfb"); ?></label>
        
        <div class="gfb_field-control">
            <input type="text" name="staff_name" id="staff_name" class="input-main notallowspecial" value="<?php echo esc_attr($staffDetail[0]['staff_name']); ?>" maxlength="100" />
            
            <input type="hidden" name="staff_id" id="staff_id" class="input-main notallowspecial" value="<?php echo esc_attr($staffDetail[0]['staff_id']); ?>" readonly="readonly" />
        </div>
    </div>
    
    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Email Address", "gfb"); ?></label>
        
        <div class="gfb_field-control">
            <input type="email" name="staff_email" id="staff_email" class="input-main" value="<?php echo esc_attr($staffDetail[0]['staff_email']); ?>" maxlength="150" />
        </div>
    </div>
    
    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Designation", "gfb"); ?></label>
        
        <div class="gfb_field-control">
            <input type="text" name="staff_designation" id="staff_designation" class="input-main notallowspecial" value="<?php echo esc_attr($staffDetail[0]['staff_designation']); ?>" maxlength="50" />
        </div>
    </div>
    
    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Contact Number", "gfb"); ?></label>
        
        <div class="gfb_field-control">
            <input type="text" name="staff_phone" id="staff_phone" class="input-main notallowspecialalpha" value="<?php echo esc_attr($staffDetail[0]['staff_phone']); ?>" maxlength="15" />
        </div>
    </div>
    
    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Info", "gfb"); ?></label>
        <div class="gfb_field-control">
            <textarea name="staff_info" id="staff_info" class="input-main" rows="4" cols="40"><?php echo wp_kses_post($staffDetail[0]['staff_info']); ?></textarea>
        </div>
    </div>
    
	<div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Transfer Appointments", "gfb"); ?></label>
        <?php
			$staff_id_data = get_user_meta($staffDetail[0]['user_id'],'transfar_staff_id');
			if(!empty($staff_id_data)){
				$staff_id_new = $staff_id_data[0];
			}else{
				$staff_id_new ="";
			}
			$options = '';
			$options .= '<option value="">Select Staff</option>';
			// The Query Select staffList
			global $wpdb;
			global $gfbStaff;
			/* Staff Id List */
			$staffList = $gfbStaff->getStaffNameList();
			// The Loop
			 if ($staffList) {
			foreach ($staffList as $resultData):
					$selected = $staff_id_new == $resultData['staff_id'] ? ' selected="selected"' : '';
					if($staffDetail[0]['staff_id'] == $resultData['staff_id']){
						
					}else{
						$options .= '<option value="'.$resultData['staff_id'].'"'.$selected.'>'.$resultData['staff_name'].'</option>' . PHP_EOL;
					}
				endforeach;
			} else {
				 //no services found
			} 
		
		?>
        <div class="gfb_field-control">
            <select type="text" name="transfar_staff_id" id="transfar_staff_id" class="input-main notallowspecialalpha"/>
				<?= $options;?>
			</select>
        </div>
    </div>
	
    <div class="gfb_field">
        <label class="gfb_field-label"><?php _e("Google Calendar Integration", "gfb"); ?></label>
        <small><p class="description"><?php _e('Synchronize staff member appointments with Google Calendar. ', 'gfb'); ?></p></small>
        
        <div class="gfb_field-control">
        
        	<?php if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	 { ?>
				<?php if(isset($auth_url)) { ?>            
                    <a  href="<?php echo $auth_url; ?>"><?php _e( 'Connect', 'gfb' ) ?></a>                
                <?php 
                } 
                else {
                        
                    $adminUrl= admin_url();
                ?>
                <label class="gfb_field-label">
                
                    <?php _e( 'Connected (', 'bookly' ) ?><a href="<?php echo $adminUrl.'admin.php?page=gravity-form-booking-staff&google_logout='.$staff_id;?>" ><?php _e( 'disconnect', 'gfb' ) ?></a><?php _e( ')', 'bookly' ) ?>
                 </label>
                
                <?php } ?>
            
            <?php } else { ?>
            	<a  href="<?php echo admin_url().'admin.php?page=gravity-form-booking-settings#google-calendar-tab'; ?>"><?php _e( 'Connect', 'gfb' ) ?></a>
            <?php } ?>
        </div>
    </div>
    
    <div class="form-group-elements">
    	<div class="form-element">
        	<?php submit_button('Save'); ?>
        </div>
    </div>

</form>