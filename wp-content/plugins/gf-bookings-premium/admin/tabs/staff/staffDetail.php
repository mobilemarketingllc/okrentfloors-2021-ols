<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	if( isset($_GET["state"]) ) {
		global $gfbStaff;	
		$staffDetail = $gfbStaff->getStaffFullDetails(base64_decode($_GET["state"]));	
	
	?>
	<div class="staffmember-box">
    
    	<div class="gfbAjaxLoader" id="gfb_loader_img">
            <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
        </div>
					
		<div class="staffmember-box-header">
			
			<div class="staffmember-intro-wrap">
				<div class="staffmember-photo">
                	<img src="<?php if( !empty($staffDetail[0]['staff_pic']) ) { echo $staffDetail[0]['staff_pic']; } else { echo GFB_ADMIN_ASSET . 'images/user-placeholder.png'; } ?>" class="image-preview" width="" height="">
                    					
					<input class="form-control" type="hidden" id="staff_pic" name="staff_pic" value="<?php echo $staffDetail[0]['staff_pic']; ?>">
                    
					<?php if( is_admin() && current_user_can( 'administrator' ) ) { ?>
                    
						<a href="#" class="gfb-image-upload-button" data-sid="<?php echo esc_attr($staffDetail[0]['staff_id']); ?>"><?php _e("Change", "gfb"); ?></a>
                    <?php } ?>
				</div>
				<div class="staffmember-intro">
					<div class="staffmember-intro-name"><?php echo esc_attr($staffDetail[0]['staff_name']); ?></div>
					<div class="staffmember-intro-meta"><?php echo esc_attr($staffDetail[0]['staff_designation']); ?></div>
                    
                   	<?php if( is_admin() && current_user_can( 'administrator' ) ) { ?>
                    <div class="staffmember-intro-meta staffmember-delete"><a href="#" data-staffid="<?php echo esc_attr($staffDetail[0]['staff_id']); ?>" class="staff-delete"><?php _e("Delete Staff Member", "gfb"); ?></a></div>					
                    <?php } ?>
				</div>
			</div>
			
			<?php
			$timing_tab_active = '';
			$detail_tab_active = '';
			if ( isset( $_GET['tab'] ) && 'timings-tab' === sanitize_text_field($_GET['tab']) ) {
				$timing_tab_active = 'active';
			} else {
				$detail_tab_active = 'active';
			}
			?>

			<div class="staffmember-box-tab-titles">
				<ul>
					<li class="<?php echo $detail_tab_active; ?>"><a href="#details-tab"><span class="dashicons dashicons-welcome-write-blog"></span><?php _e(" Details", "gfb"); ?></a></li>
					<li><a href="#services-tab"><span class="dashicons dashicons-admin-settings"></span><?php _e(" Services", "gfb"); ?></a></li>
					<li class="<?php echo $timing_tab_active; ?>"><a href="#timings-tab"><span class="dashicons dashicons-clock"></span><?php _e(" Timings", "gfb"); ?></a></li>
					<li><a href="#daysoff-tab"><span class="dashicons dashicons-calendar-alt"></span><?php _e(" Daysoff", "gfb"); ?></a></li>
				</ul>
			</div>
	
		</div>
	
		<div class="staffmember-box-tab-content">     
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>   	
			
			<div class="staffmember-box-tab <?php echo $detail_tab_active; ?>" id="details-tab">        
				<?php require( GFB_ADMIN_TAB . 'staff/tabStaffDetails.php' ); ?>             
			</div>
	
			<div class="staffmember-box-tab" id="services-tab">
				<?php require( GFB_ADMIN_TAB . 'staff/tabStaffServices.php' ); ?>             
			</div>
	
			<div class="staffmember-box-tab <?php echo $timing_tab_active; ?>" id="timings-tab">
            	<?php require( GFB_ADMIN_TAB . 'staff/tabStaffSlots.php' ); ?>
			</div>
	
			<div class="staffmember-box-tab" id="daysoff-tab">
            	<?php require( GFB_ADMIN_TAB . 'staff/tabStaffDaysoff.php' ); ?> 
			</div>
	
		</div>
	
	</div>
	<?php


}
