<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Category_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'category',     //singular name of the listed records
				'plural'    => 'categories',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		function get_columns()
		{
			$columnNames = array(
				'cb' => '<input type="checkbox" />',
				'category_name' => 'Category Name',
				'edit' => 'Edit',
				'delete' => 'Delete'				
			);
			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{
			switch( $column_name ) 
			{ 				
				case 'category_name':
					return $item[ $column_name ];
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function column_cb($item)
		{
			return sprintf( '<input type="checkbox" name="category_id[]" value="%s">',$item['category_id']);
		}
		
		function column_delete($item)
		{
			if( isset($_REQUEST['paged']) ) {
				$paged = $_REQUEST['paged'];	
			}
			else {
				$paged = 1;
			}
			
			$actions = array(
				'delete' => sprintf('<a data-catid="%s" href="#" class="gfb_grid-btn btn-red gfb_delete_category"><span class="dashicons dashicons-no"></span></a>', $item['category_id']),
			);
	
			return $this->row_actions($actions, true);
		}
		
		function column_edit($item)
		{
			$actions = array(
				'edit' => sprintf('<a data-catid="%s" href="#editCategoryHtml" name="edit_category" id="edit_category" class="gfb_grid-btn btn-blue edit-category-modal"><span class="dashicons dashicons-edit"></span></a>', $item['category_id']),
			);
			
			return sprintf('%1$s', $this->row_actions($actions, true) );
		}
		
		function get_bulk_actions()
		{
			$actions = array(
				'delete' => 'Delete',
			);
			return $actions;
		}
		
		function process_bulk_action()
		{
			global $wpdb;
			$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";

			if ('delete' === $this->current_action()) {
				$ids = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : array();
				if (is_array($ids)) $ids = implode(',', $ids);
	
				if (!empty($ids)) {

					$deleteResult = $wpdb->query("UPDATE ".$gfb_categories_mst." SET is_deleted=1 WHERE category_id IN(".$ids.")");
				
					$plugins_url = admin_url().'admin.php?page=gravity-form-booking-categories&paged='.$_REQUEST['paged'];
					
					if($deleteResult)
					{
						echo "<script>jQuery(document).ready(function() { window.location.replace('".$plugins_url."'); });</script>";
					}
					unset($deleteResult);
				}
			}
			
		}
		
		function get_sortable_columns() {}
		
		function prepare_items() 
		{
			global $wpdb;
			$perPage = 7;
			$currentPage = $this->get_pagenum();
			
			$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
			
			if( isset($_GET['categorynm-filter']) && $_GET['categorynm-filter'] <> '' ) {
				$filter = ' AND category_name LIKE "%'.$_GET['categorynm-filter'].'%"';			
			}
			else {				
				$filter = '';
			}
			
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->process_bulk_action();
			
			$category_results = $wpdb->get_results("SELECT category_id, category_name FROM ".$gfb_categories_mst." WHERE is_deleted=0 ".$filter." ORDER BY category_name asc", ARRAY_A);
			
			$totalItems = count($category_results);
		
			$category_rows = array_slice($category_results,(($currentPage-1)*$perPage),$perPage);
			
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage) // calculate pages count
			));
			
			$this->items = $category_rows;
		}
	}