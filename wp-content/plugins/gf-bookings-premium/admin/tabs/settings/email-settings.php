<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



global $gfbSettingsObj;
?>
<div class="tab-section">

    <div class="tab-title">
        <h4 class="gfb_section-title"><?php _e('Email Configuration', 'gfb'); ?></h4>
    </div>
    
    <div class="tab-body">  
        <form name="email_setting_form" id="email_setting_form" action="#" method="post">
            
            <div class="form-section">
        
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_sender_name"><?php _e('Sender Name : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_sender_name" id="gfb_sender_name" class="input-main" placeholder="<?php echo get_bloginfo( 'name' ); ?>" value="<?php echo get_option('gfb_sender_name'); ?>" maxlength="100" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_sender_email"><?php _e('Sender Email : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="email" name="gfb_sender_email" id="gfb_sender_email" class="input-main" placeholder="example@domain.com" value="<?php echo get_option('gfb_sender_email'); ?>" maxlength="150" />
                        <p class="description"><?php _e("Enter email address from which sender will send the mail. Email should be of domain.", "gfb"); ?></p>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                        <?php submit_button('Save Email Settings'); ?>
                    </div>
                </div>
                
            </div>
        </form>
      
    </div>
   
</div> 