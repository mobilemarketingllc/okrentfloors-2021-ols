<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

//Check Verification
$res = GravityFormBookingCheckVerification::gfbGetCheckPluginVerification();		
			
if( $res ) {

?>

<div class="main-container">

	<div class="holiday-section">
    
    	<div class="form-section">
            
            <div class="form-group-elements">
        
                <div class="form-element">
                    <label>
                    	<input type="checkbox" name="gfb_add_holiday" id="gfb_add_holiday" />
          				<?php _e("We are not working on this day.", "gfb"); ?>
                    </label>
                </div>
            
            </div>
            
            <div class="form-group-elements">
        
                <div class="form-element">
                	<label>
                        <input type="checkbox" name="gfb_repeat_holiday" id="gfb_repeat_holiday" disabled="disabled" />
                        <?php _e("Repeat every year.", "gfb"); ?>
                    </label>
                </div>
            
            </div>
        
        </div>
               
    </div>
    
</div>
<?php

}