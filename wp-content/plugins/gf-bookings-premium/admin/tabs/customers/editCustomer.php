<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
$customerObj = new GravityFormBookingCustomer();
$customerDetails = $customerObj->gfbCustomerDetails($_POST['customer_id']);
	
?>     
<form name="edit_customer_form" class="customer-form" id="edit_customer_form" method="post"> 

    <div class="form-section">
    
    	<div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="customer_name"><?php _e('Customer Name', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="customer_name" id="customer_name" class="input-main notallowspecial" value="<?php echo $customerDetails[0]['customer_name']; ?>" maxlength="200" />
            </div>
            <div class="form-element">
                <input type="hidden" name="customer_id" id="customer_id" class="input-main notallowspecial" value="<?php echo $customerDetails[0]['customer_id']; ?>" readonly="readonly" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="customer_email"><?php _e('Customer Email', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="customer_email" id="customer_email" class="input-main" value="<?php echo $customerDetails[0]['customer_email']; ?>" maxlength="150" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="customer_contact"><?php _e('Customer Contact No', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="customer_contact" id="customer_contact" class="input-main notallowspecialalpha" value="<?php echo $customerDetails[0]['customer_contact']; ?>" maxlength="15" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
            <div class="form-element">
                <?php wp_nonce_field('edit_customer_nonce_field', 'edit_customer_nonce', true, true); ?>
                <?php submit_button('Update Customer'); ?>
            </div>
        </div>
        
    </div>
    
</form>