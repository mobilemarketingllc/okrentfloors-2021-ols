(function( $ ){
	$(document).ready(function(){
		
		/* Payment Detail Chart */
		var paymentChart = new Chart("paymentChartDiv", {
			type: 'pie',
			data: {
				labels: $.parseJSON(js_chart_ajax.getpaymentdetails_type),
				datasets: [{
					data: $.parseJSON(js_chart_ajax.getpaymentdetails_amount),
					backgroundColor: [
						'rgba(255, 99, 132, 0.6)',
						'rgba(54, 162, 235, 0.6)',
						'rgba(255, 206, 86, 0.6)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)'
					],
					borderWidth: 1,
				}]
			},
			options: {
				responsive: true
			}
		});
		
		/* Appointment Status Chart */
		var appointmentStatusChart = new Chart("appointmentStatusChartDiv", {
			type: 'doughnut',
			data: {
				labels: $.parseJSON(js_chart_ajax.getappointmentstatus_status),
				datasets: [{
					data:  $.parseJSON(js_chart_ajax.getappointmentstatus_appoint),
					backgroundColor: [
						'rgba(75, 192, 192, 0.6)',
						'rgba(54, 162, 235, 0.6)',
						'rgba(255, 206, 86, 0.6)',
						'rgba(255,99,132, 0.6)',
					],
					borderColor: [
						'rgba(75, 192, 192, 1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(255,99,132, 1)',
					],
					borderWidth: 1,
				}]
			},
			options: {
				responsive: true
			}
		});
				
		/* Service wise total no. of booking */		
		var bookingServiceChart = new Chart("bookingChartService", {
			type: 'horizontalBar',
			data: {
				labels: $.parseJSON(js_chart_ajax.ear_service),
				datasets: [{
					label: 'Total Earnings',
					backgroundColor: "rgba(99,189,249, 0.6)",
					borderColor: "rgba(99,189,249, 1)",
					borderWidth: 1,
					data: $.parseJSON(js_chart_ajax.amt_service),
				}]
			},
			options: {
				layout: {
					padding: {
						left: 50,
						right: 0,
						top: 0,
						bottom: 0
					}
				},
				responsive: true,
				legend: {
					position: 'top',
				},
				scales: {
					xAxes: [{
						stacked: true,
						maxBarThickness:60,
					}],
					 yAxes: [{
						stacked: true
					}]
				}
			}
		});
		
		/* Service wise earning */
		var earningServiceChart = new Chart("earningChartService", {
			type: 'bar',
			data: {
				labels: $.parseJSON(js_chart_ajax.ear_service),
				datasets: [{
					label: 'Total No. of Appointments',
					backgroundColor: "rgba(249,155,175, 0.6)",
					borderColor: "rgba(249,155,175, 1)",
					borderWidth: 1,
					data: $.parseJSON(js_chart_ajax.app_service),
				}]
			},
			options: {
				layout: {
					padding: {
						left: 50,
						right: 0,
						top: 0,
						bottom: 0
					}
				},
				responsive: true,
				legend: {
					position: 'top',
				},
				scales: {
					xAxes: [{
						stacked: true,
						maxBarThickness:60,
					}],
					 yAxes: [{
						stacked: true
					}]
				}
			}
		});
		
		/* Staff wise Earnings */		
		var earningStaffChart = new Chart("earningChartStaff", {
			type: 'line',
			data: {
				labels: $.parseJSON(js_chart_ajax.getStaffDataChart_staff),
				datasets: [{
					label: 'Total Earnings',
					backgroundColor: "rgba(255, 206, 86, 0.6)",
					borderColor: "rgba(255, 206, 86, 1)",
					fill:true,
					borderWidth: 1,
					data: $.parseJSON(js_chart_ajax.getStaffDataChart_amount)
				}]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				scales: {
					responsive: true,
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				} 
				}
			}
		});
		
		
		/* Staff wise total no. of booking */		
		var bookingStaffChart = new Chart("bookingChartStaff", {
			type: 'line',
			data: {
				labels: $.parseJSON(js_chart_ajax.getStaffDataChart_staff),
				datasets: [{
					label: 'Total No. of Appointments',
					backgroundColor: "rgba(141,211,211, 1)",
					borderColor: "rgba(141,211,211, 1)",
					borderWidth: 1,
					fill: true,
					data: $.parseJSON(js_chart_ajax.getStaffDataChart_appointments),
				}],
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				scales: {
					responsive: true,
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				} 
				}
			}
		});

	});
}(jQuery));