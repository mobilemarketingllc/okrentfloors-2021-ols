(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
	
	var $notification_form = $("#app_notification_form"),
		$submitbtn = jQuery('.email-notify-submit input'),
		$gfb_loader_img = $("#gfb_loader_img");
	
		$( "#accordion" ).accordion({
			collapsible: true,
			heightStyle: "content",
			animate: 200	
		});		
		
		$submitbtn.mousedown( function() {
			tinyMCE.triggerSave();
		});			
			
	
		$notification_form.on("submit", function(e){
			
			e.preventDefault();	
			
			$notification_form.addClass("loading-block");
			
			var dataVal = {
				action 	  : "gfb_add_mail",
				form_data : $(this).serialize()	
			};
						
			jQuery.post(
				js_email_notify_ajax.ajaxurl, dataVal, function(response) {

					var obj=$.parseJSON(response);
					
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" }, function(){
							$notification_form.removeClass("loading-block");
						});
					}	
					else {
						swal({ title:obj.text, type:"error" });	
						$notification_form.removeClass("loading-block");
					}							
				}
			);
			
			
		});	
		
	});
}(jQuery));