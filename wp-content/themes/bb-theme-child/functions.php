<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
	 wp_enqueue_script("script",get_stylesheet_directory_uri()."/script.js","","",11);
    
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( @$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring *<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo $_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php 
}

//check out flooring FUnctionality

add_action( 'wp_ajax_nopriv_check_out_flooring', 'check_out_flooring' );
add_action( 'wp_ajax_check_out_flooring', 'check_out_flooring' );

function check_out_flooring() {

    $arg = array();
    if($_POST['product']=='carpeting'){

        $posttype = array('carpeting');

    }elseif($_POST['product']=='hardwood_catalog,laminate_catalog,luxury_vinyl_tile'){

        $posttype = array('hardwood_catalog','laminate_catalog','luxury_vinyl_tile');

    }elseif($_POST['product']=='tile_catalog' || $_POST['product']=='tile'){

        // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
        $posttype = array('tile_catalog');
    }

    $parameters =  explode(",",$_POST['imp_thing']);

foreach($parameters as $para){
     if($para == 'hypoallergenic'){

        $arg['hypoallergenic'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50
        );
     }elseif($para == 'diy_friendly'){

        $arg['diy_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'installation_facet',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'installation_method',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
            ),             
        );
     }elseif($para == 'aesthetic_beauty'){

        $arg['aesthetic_beauty'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'brand',
                    'value'   => 'Anderson Tuftex',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'brand',
                    'value'   => 'Karastan',
                    'compare' => '=',
                    
                ),
            ),
        );
     }elseif($para == 'eco_friendly'){
        $arg['eco_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Eco',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );
     }
     elseif($para == 'pet_friendly'){

        $arg['pet_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Pet',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );

        
     }elseif($para == 'easy_to_clean'){
        
        $arg['easy_to_clean']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'durability'){
        
        $arg['durability']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }
     elseif($para == ''){
        
        $arg['colors']=array(
            'post_type'      => array( 'luxury_vinyl_tile', 'laminate_catalog','hardwood_catalog','carpeting','tile' ),
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
        );

        
     }
     elseif($para == 'stain_resistant'){
        
        $arg['stain_resistant'] =array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Stain Resistant',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'Collection',
                    'value'   => 'Bellera',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'budget_friendly'){
        
        $arg['budget_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'fiber',
                    'value'   => array( 'SmartStrand','Stainmaster','Bellera','Pet R2x' ),
                    'compare' => 'IN',
                    
                ),
            ),
        );

        
     }elseif($para == 'color'){
        
        $arg['color'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50            
        );

        
     }

}
 
     $i = 1;

     $find_posts = array();
     foreach($parameters as $para){
        $wp_query[$i] = new WP_Query($arg[$para]);
        
        
        $find_posts = array_merge( $find_posts , $wp_query[$i]->posts);        

        $i++;
     }
     
     //print_r($find_posts);
     if (!$find_posts ) {
       
            $args =array(
                'post_type'      => $posttype,
                'post_status'    => 'publish',
                'posts_per_page' => 50,
                'orderby'        => 'rand'
            );
            $find_posts   = new WP_Query($args);
           // var_dump($args , $find_posts);
            $find_posts = $find_posts->posts;
     }

     if ( $find_posts ) {
          
        $content ="";

        foreach ( $find_posts as $post ) {

            $image = swatch_image_product_thumbnail($post->ID,'222','222');
            $sku = get_field( "sku", $post->ID );
           
     $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="'.$image.'" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>'.get_field( "collection", $post->ID ).'</h4> 
                                <h3>'.get_field( "color", $post->ID ).'</h3>
                                <p class="brand"><small><i>'.get_field( "brand", $post->ID ).'</i></small></p>
                                <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
                                Order sample
                                </a> -->
                                <a class="prodLink" href="'.get_permalink($post->ID).'">See info</a>
                            </div>
                            
                        </div>
                    </div>';
        }

        $content .='<div class="text-center buttonWrap" data-search="'.$_POST['imp_thing'].'">
                    <a class="button" href="javascript:void(0)">view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div>';

    }

    echo $content;
    wp_die();
}



  //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {
	
	if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile-and-stone/',
            'text' => 'Tile & Stone',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile-and-stone/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
	
}

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}
add_filter( 'auto_update_plugin', '__return_false' );



 // Vendor page product list

function search_distinct() {    
    
    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';  
}



function brand_products_function($atts){

    global $wpdb;

    // "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
    //write_log( $atts);
    $args = array(
        "post_type" =>  array('luxury_vinyl_tile'),
        "post_status" => "publish",       
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value', 
        "posts_per_page" => 4,
      'meta_query' => array(
                array(
                  'key' => 'brand',
                  'value' => $atts[0],
                  'compare' => '='
                  )
         )
      );

    //  write_log( $args);
    add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';				
                                
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                            if( get_field('quick_ship') == 1){ 
  
                              $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';
  
                                  } 
  
                         $content .='</a></div>';

                    $table_posts = $wpdb->prefix.'posts';
                    $table_meta = $wpdb->prefix.'postmeta';
                    $familycolor = get_field('collection');  
                    $key = 'collection';  

                    $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

                           //     write_log($coll_sql);

                    $data_collection = $wpdb->get_results($coll_sql);  

    
    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"> <span class="color_text">'.get_field('color').'</span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>'.count($data_collection).' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
           
    } 
       
    wp_reset_postdata(); 
    
    remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';
            
            return $content;
    
    } 
    
    add_shortcode( 'brand_listing', 'brand_products_function' );

    
function carpet_brand_products_function($atts){

    global $wpdb;

    // "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
    //write_log( $atts);
    $args = array(
        "post_type" =>  array('carpeting'),
        "post_status" => "publish",       
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value', 
        "posts_per_page" => 4,
      'meta_query' => array(
                array(
                  'key' => 'brand',
                  'value' => $atts[0],
                  'compare' => '='
                  )
         )
      );

    //  write_log( $args);
    add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';				
                                
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                            if( get_field('quick_ship') == 1){ 
  
                              $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';
  
                                  } 
  
                         $content .='</a></div>';

                    $table_posts = $wpdb->prefix.'posts';
                    $table_meta = $wpdb->prefix.'postmeta';
                    $familycolor = get_field('collection');  
                    $key = 'collection';  

                    $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

                           //     write_log($coll_sql);

                    $data_collection = $wpdb->get_results($coll_sql);  

    
    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"> <span class="color_text">'.get_field('color').'</span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>'.count($data_collection).' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
           
    } 
       
    wp_reset_postdata(); 
    
    remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';
            
            return $content;
    
    } 
    
   add_shortcode( 'carpet_brand_listing', 'carpet_brand_products_function' );



add_filter( 'gform_field_validation_39_32', 'custom_validation', 20, 4 );


function custom_validation( $result, $value, $form, $field ) {
	if ( $result['is_valid'] ) {
    $acceptable_zips = array (
  0 => '78070',
  1 => '78015',
  2 => '78247',
  3 => '78259',
  4 => '78266',
  5 => '78248',
  6 => '78216',
  7 => '78217',
  8 => '78209',
  9 => '78213',
  10 => '78230',
  11 => '78231',
  12 => '78232',
  13 => '78239',
  14 => '78148',
  15 => '78154',
  16 => '78258',
  17 => '78132',
  18 => '78109',
  19 => '78260',
  20 => '78261',
  21 => '78108',
  22 => '78163',
  23 => '78130',
  24 => '78133'
);

    // $zip_value = rgar( $value, $field->id . '20' );
// var_dump($value,$zip_value, $acceptable_zips);
    if ( !in_array( $value, $acceptable_zips ) ) {
        $result['is_valid'] = false;
        $result['message']  = "Unfortunately we are unable to schedule an appointment in your area at this time.";
    } else {
		$result['is_valid'] = true;
        $result['message']  = 'Good News! We are able to schedule an appointment to your area.';
	}
}

return $result;
}

//Cron job for sync catalog for all mohawk categories

if (! wp_next_scheduled ( 'sync_mohawk_catalog_all_categories')) {
  
    wp_schedule_event( strtotime("last Sunday of ".date('M')." ".date('Y').""), 'monthly', 'sync_mohawk_catalog_all_categories');
}


//add_action( 'sync_mohawk_catalog_all_categories', 'mohawk_product_sync', 10, 2 );

function mohawk_product_sync(){

    write_log("Only mohawk Catalog sync is running"); 

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  
    
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	
    $product_json =  json_decode(get_option('product_json'));     

    $brandmapping = array(
        "hardwood"=>"hardwood_catalog",
        "laminate"=>"laminate_catalog",
        "lvt"=>"luxury_vinyl_tile",
        "tile"=>"tile_catalog"      
    );

    foreach($brandmapping as $key => $value){               
       
            $productcat_array = getArrayFiltered('productType',$key,$product_json);               

            
            foreach ($productcat_array as $procat){

                if($procat->manufacturer == "Mohawk"){

                    $permfile = $upload_dir.'/'.$value.'_'.$procat->manufacturer.'.json';
                    $res = SOURCEURL.get_option('SITE_CODE').'/www/'.$key.'/'.$procat->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
                    $tmpfile = download_url( $res, $timeout = 900 );

                        if(is_file($tmpfile)){
                            copy( $tmpfile, $permfile );
                            unlink( $tmpfile ); 
                        } 

                          $sql_delete = "DELETE p, pm FROM $table_posts p INNER JOIN $table_meta pm ON pm.post_id = p.ID  WHERE p.post_type = '$value' AND pm.meta_key = 'manufacturer' AND  pm.meta_value = 'Mohawk'" ;						
                          write_log($sql_delete); 
                          $delete_endpoint = $wpdb->get_results($sql_delete);
                          write_log("mohawk product deleted"); 
                          //exit;

                        write_log('auto_sync - Shaw catalog - '.$key.'-'.$procat->manufacturer);
                       $obj = new Example_Background_Processing();
                       $obj->handle_all($value, $procat->manufacturer);

                        write_log('Sync Completed - '.$procat->manufacturer);

                  }
                    
                }
                
                   write_log('Sync Completed for all  '.$key.' brand');
        }                
}

